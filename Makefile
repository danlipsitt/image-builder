CFG = typea-series1-wheezy
CFG_FILE = configs/$(CFG).conf
BASEDIR ?= $(shell pwd)
IB_OUTDIR ?= $(BASEDIR)/deploy
BUILD_DATE = $(shell date +%Y-%m-%d)
APT_CACHE ?= ""
NAME_PREFIX := $(shell scripts/echo-output-filename-prefix.sh $(CFG_FILE))
OUT_NAME = "deploy/$(NAME_PREFIX)-$(BUILD_DATE)"

rootfs: $(OUT_NAME)
$(OUT_NAME):
	/usr/bin/time ./RootStock-NG.sh -c $(CFG_FILE)

img: $(OUT_NAME).img
$(OUT_NAME).img: $(OUT_NAME)
	$(BASEDIR)/giftwrap.sh $<

docker_build:
	docker build -t image-builder:x86 .

docker_run:
	if [ ! -d $(IB_OUTDIR) ]; then mkdir $(IB_OUTDIR); fi
	docker run --privileged --env http_proxy="${APT_CACHE}" \
	  -ti --rm -v $(IB_OUTDIR):/app/deploy image-builder:x86

cleanall:
	rm -r $(BASEDIR)/deploy
