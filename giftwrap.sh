#!/bin/bash
set -x
set -e
set -u

pre_generic_img () {
    if [ -d "./${base_rootfs}" ] ; then
        rm -rf "${base_rootfs}" || true
    fi

    if [ -f "${base_rootfs}.tar.xz" ] ; then
        tar xf "${base_rootfs}.tar.xz" -C deploy/
    else
        tar xf "${base_rootfs}.tar" -C deploy/
    fi
}

generic_img () {
    local base_rootfs=$1
    local options=$2
    (
	cd "${base_rootfs}"
	sudo ../../tools/setup_sdcard.sh ${options}
	mv ./*.img ../
    )
}

post_generic_img () {
    ## archive *.tar
    local base_rootfs=$1
    if [ -d "./${base_rootfs}" ] ; then
        rm -rf "${base_rootfs}" || true
    fi

    if [ ! -f "${base_rootfs}.tar.xz" ] ; then
        xz -z -8 -v "${base_rootfs}.tar"
    fi
}

compress_img () {
    local wfile=$1
    if [ -f "${wfile}" ] ; then
        #prevent xz warning for 'Cannot set the file group: Operation not permitted'
        sudo chown "${UID}:${GROUPS}" "${wfile}"
        xz -z -8 -v "${wfile}"
    fi
}

main() {
	local base_rootfs=$1
	local build_name="${1##*/}"
	local options="\
		--img-2gb ${build_name} \
		--dtb beaglebone \
		--beagleboard.org-production \
		--boot_label series1 \
		--enable-systemd \
		--bbb-old-bootloader-in-emmc
		--hostname series1-unconfigured"

	pre_generic_img "$base_rootfs"
	generic_img "$base_rootfs" "$options"
	post_generic_img "$base_rootfs"
	compress_img "${base_rootfs}.img"
}

main "$@"
