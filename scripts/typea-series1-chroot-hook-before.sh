#!/bin/sh
set -x
set -e
set -u

sudo cp -v ${OIB_DIR}/target/sources.list.d/typea-series1*.list \
	 ${tempdir}/etc/apt/sources.list.d/

return 0
