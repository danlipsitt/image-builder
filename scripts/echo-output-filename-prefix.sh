#!/bin/sh

# This is a utility to get the output filename from a config because
# make can't source bash scripts

. $1
filename_prefix="${deb_distribution}-${release}-${image_type}-${deb_arch}"
echo $filename_prefix
